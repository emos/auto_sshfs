#!/usr/bin/python

import sys
import os
import json
import getopt
from sys import platform as _platform
from inspect import currentframe, getframeinfo
from subprocess import Popen, PIPE


DEFAULT_CONFIG_LOCATION = [r"~/.auto_sshfs.conf", \
                           r"/etc/default/auto_sshfs.conf"]

if _platform == "darwin": # OSX
    SSHFS_CMD  = "/usr/local/bin/sshfs"
    MOUNT_CMD  = "/sbin/mount"
    UMOUNT_CMD = "/sbin/umount"
else:
    SSHFS_CMD  = "/usr/bin/sshfs"
    MOUNT_CMD  = "/bin/mount"
    UMOUNT_CMD = "/bin/fusermount -u"
	
def usage(prog_name, error):
    print("Auto-mount directories using sshfs")
    print(error)
    print("Usage: %s -c <JSON configuration file> [-u]" % (prog_name,))
    sys.exit(0)

# prints a message to stdout + stderr, adds the source file + line number.
# use this instead of echo
def msg(s):
    frameinfo = getframeinfo(currentframe().f_back)
    f = os.path.basename(frameinfo.filename)
    l = frameinfo.lineno
    print("[\x1b[34m%s:%d\x1b[39m] %s" % (f,l,s))

def mount(local_dir, host, remote_dir, user, allow_other):
    cmd = []
    # add command and path
    cmd.extend([SSHFS_CMD])

    # add parameters
    cmd.extend(["-o", "reconnect"])
    
    if _platform == "darwin": # OSX
        cmd.extend(["-o", "volname=%s" % (local_dir)])

    if allow_other is not None and allow_other:
	    cmd.extend(["-o", "allow_other"])    
    cmd.extend(["-o", "ServerAliveInterval=60"])
    cmd.extend(["-o", "Ciphers=arcfour"])
    cmd.extend(["-o", "no_readahead"])
    
    if _platform == "darwin": # OSX
        cmd.extend(["-o", "noappledouble"])
        cmd.extend(["-o", "nolocalcaches"])
    elif _platform == "linux" or _platform == "linux2":
        pass

    # remote dir
    cmd.extend(["%s@%s:%s" % (user, host, remote_dir),])

    # local dir
    cmd.extend([local_dir])

    # start mounting
    msg("Mounting %s@%s:%s to %s" % (user, host, remote_dir, local_dir))
    process = Popen(cmd, stdout=PIPE, stderr=None)
    process.communicate()[0]

def umount(local_dir):
    # start umounting
    msg("Unmounting %s" % local_dir)
    cmd = UMOUNT_CMD.split()
    cmd.extend([local_dir])
    process = Popen(cmd, stdout=PIPE, stderr=None)
    process.communicate()[0]

def is_mounted(local_dir):
    cmd = [MOUNT_CMD]
    process = Popen(cmd, stdout=PIPE, stderr=None)
    for line in process.communicate()[0].split("\n"):
        if local_dir in line:
            return True
    return False

def mount_all(config):
    for mnt in config["mounts"]:
        mnt_path = os.path.join(config["local_base_path"], mnt["local_dir"])
        if not os.path.exists(mnt_path):
            os.makedirs(mnt_path)
            msg("Warning: %s was created!" % (mnt_path,))
        if is_mounted(mnt_path):
            msg("Error: %s is already mounted!" % (mnt_path,))
            continue
        if os.listdir(mnt_path):
            msg("Error: %s is not empty!" % (mnt_path,))
            continue
        mount(mnt_path, mnt["host"], mnt["remote_dir"], mnt["user"], mnt.get("allow_other"))

def umount_all(config):
    for mnt in config["mounts"]:
        mnt_path = os.path.join(config["local_base_path"], mnt["local_dir"])
        if not is_mounted(mnt_path):
            msg("Error: %s is not mounted!" % (mnt_path,))
            continue
        umount(mnt_path)

if __name__ == '__main__':
    config_filenames = DEFAULT_CONFIG_LOCATION
    operation = "mount"
    filters = None
    if len(sys.argv) > 1:
        try:
            opts, args = getopt.getopt(sys.argv[1:], "c:huf:", ["conf=", "help", "umount", "filter="])
        except getopt.GetoptError:
            usage(sys.argv[0], getopt.GetoptError.msg)

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                usage(sys.argv[0], "Help")
            elif opt in ("-c", "--conf"):
                msg("configuration file: %s" % (arg, ))
                config_filenames = [arg]
	    elif opt in ("-u", "--umount"):
                operation = "umount"
            elif opt in ("-f", "--filter"):
                filters = arg.split(",")

    config = {}
    for filename in config_filenames:
        msg("configuration file: %s" % (filename, ))
        filename = os.path.expanduser(filename)
        msg("configuration file: %s" % (filename, ))
        if not os.path.exists(filename):
            msg("False")
            continue
        msg("using configuration file: %s" % (filename, ))
        with open(filename, "r") as f:
            config = json.load(f)
	break

    if not config:
        usage(sys.argv[0], "Configuration file not found!")

    if filters is not None:
        config["mounts"] = [mnt for mnt in config["mounts"] \
                            if len([filter for filter in filters if filter in mnt["local_dir"]])]

    if operation == "mount":
        mount_all(config)
    else:
        umount_all(config)
