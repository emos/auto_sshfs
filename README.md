auto_sshfs  - a simple automatic mouting script using sshfs 
===========================================================

Usage: auto_sshfs.py

If you want an alternative configuration file use: 

auto_sshfs.py -c <JSON configuration file> [--umount] [--filter <name,...>]

--filter: filter by "local_dir"

## Configuration File: ##

Location for single user: ~/.auto_sshfs.conf

Location for all users: /etc/default/auto_sshfs.conf

```
#!json

{
  "local_base_path" : "/path/to/local/directory",
  "mounts" : [
    { 
      "host": "my.remote.host.com", 
      "local_dir":"local_directory_name", 
      "remote_dir":"/path/to/directory/in/remote/host", 
      "user":"my_user_name_at_remote_host"
      "allow_other":true/false [OPTIONAL]
    }
  ]
}

```
### JSON Configuration file parameters: ###
* "local_base_path" : base local directory for mounting
* "mounts": list of directories to mount
* "host": remote host name 
* "local_dir": local directory, a relative path to "local_base_path"
* "remote_dir": directory at remote host
* "user": user name at remote host
* "allow_other": allow other users to list the content of the mounted directory

## What is this repository for? ##

The script and a configuration file example.

## How do I get set up? ##
```
#!bash

# Clone the repository
git clone git@bitbucket.org:emos/auto_sshfs.git auto_sshfs
cd auto_sshfs
```

## What is to configure to enable allow_other option? ##
Edit the file /etc/fuse.conf and uncomment "user_allow_other". Make sure you have
sufficient privileges to access (read) the latter file. One way is to add yourself
to "fuse" group.

## Installation instructions: ##
```
#!bash

sudo cp auto_sshfs/auto_sshfs.py /usr/bin/auto_sshfs
# start the mount on system startup
sudo cp /etc/rc.local /etc/rc.local.backup
# add the following line to rc.local: su -c "/usr/bin/auto_sshfs" MYUSER
sudo sed -i '$ d' /etc/rc.local
sudo echo 'su -c "/usr/bin/auto_sshfs" $USER' >> /etc/rc.local
sudo echo 'exit 0' >> /etc/rc.local
```

## Dependencies (on Debian): ##
```
#!bash

sudo apt-get install sshfs
```
## Contribution guidelines ##

* Don't break the script.
* Don't add auto-generated files, backup file, etc...

## Who do I talk to? ##

* Eyal Moscovici
